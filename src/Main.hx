package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.Lib;

/**
 * ...
 * @author eg
 */

class Main extends Sprite 
{
	var inited:Bool;
	
	public var ball:Ball;
	public var line:RedLine;
	public var mytarget:MyTarget;
	public var eventOverline:Event;
	public var startTime:Float;
	public var endTime:Float;
	public var startX:Float;
	public var startY:Float;
	public var endX:Float;
	public var endY:Float;
	
	public var allwidth : Float;
	public var allheight : Float;
	
	public var prevTime:Float;
	
	public var state:Int = 0;
	
	public var startBorder:Int = 65;
	
	public var replay : ReplayButton;
	

	/* ENTRY POINT */
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;

		// (your code here)
		ball = new Ball();
		addChild(ball);
//		line = new RedLine(stage.stageWidth);
//		line.y = 65;
//		addChild(line);
		mytarget = new MyTarget();
		mytarget.x = Math.random() * stage.stageWidth;
		mytarget.y = stage.stageHeight - 50 * Math.random() - 22;
		addChild(mytarget);
		ball.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		ball.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		ball.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		
		allwidth = stage.stageWidth ;
		allheight = stage.stageHeight;
		
		replay = new ReplayButton();
		replay.x = -200;
		replay.y = -200;
		replay.addEventListener(MouseEvent.CLICK, onAgain);
		addChild(replay);
		
		
//		ball.startDrag();
		// Stage:
		// stage.stageWidth x stage.stageHeight @ stage.dpiScale
		
		// Assets:
		// nme.Assets.getBitmapData("img/assetname.jpg");
	}

	/* SETUP */

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public function onMouseDown(e:MouseEvent):Void
	{
		ball.startDrag();
		ball.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		ball.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		startX = ball.x;
		startY = ball.y;
		startTime = Lib.getTimer();
	}
	public function onMouseUp(e:MouseEvent):Void
	{
		ball.stopDrag();
		ball.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		ball.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
	}

	public function onAgain(e:MouseEvent):Void
	{
		
//		trace('again');
		replay.x = -200;
		replay.y = -200;
		ball.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		this.removeChild(this.getChildByName('blooop'));
		ball.x = 0;
		ball.y = 0;
		state = 0;
		mytarget.x = Math.random() * allwidth;
		mytarget.y =  allheight - 50 * Math.random() - 22;
		ball.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		
	}
	public function onEnterFrame(e:Event):Void
	{
		if (state == 0) {
			if (ball.y >= startBorder)
			{
//				Lib.trace('hit!!');
//trace('hit');
				ball.stopDrag();
				endX = ball.x;
				endY = ball.y;
				endTime = Lib.getTimer();
				state = 1;
				prevTime = Lib.getTimer();
				
			}			
			
		}

		if (state == 1) {
			var nextTime:Float = Lib.getTimer();
			ball.x += ((endX - startX) / (endTime - startTime)) * (nextTime - prevTime);
			
			ball.y += ((endY - startY) / (endTime - startTime)) * (nextTime - prevTime);
			
			prevTime = nextTime;
			
			if ((ball.y > allheight) || (ball.x > allwidth) || (ball.x < 0) || ( ball.y < 0))
			{
				ball.x = 0;
				ball.y = 0;
				state = 0;
				mytarget.x = Math.random() * allwidth;
				mytarget.y =  allheight - 50 * Math.random() - 22;
				ball.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			}
			if (ball.hitTestObject(mytarget))
			{
				var bloop =  new Bloop();
				bloop.x = mytarget.x;
				bloop.y = mytarget.y;
				bloop.name = "blooop";
				addChild(bloop);
				state = 2;
			}
			
			
			
		}
		if (state == 2)
		{
			replay.x = allwidth / 2 - 75;
			replay.y = allheight / 2 + 15;
		}
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
	}
}
