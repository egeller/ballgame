package ;

import flash.display.Sprite;

/**
 * ...
 * @author eg
 */
class MyTarget extends Sprite
{

	public function new() 
	{
		super();
		this.graphics.beginFill(0x000000, 1);
		this.graphics.drawCircle(22, 22, 22);
		this.graphics.endFill();
		this.graphics.beginFill(0xffffff, 1);
		this.graphics.drawCircle(22, 22, 20);
		this.graphics.endFill();

	}
	
}