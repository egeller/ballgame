package ;

import flash.display.Sprite;

import flash.text.TextField;

/**
 * ...
 * @author eg
 */
class Bloop extends Sprite
{

	public function new() 
	{
		super();
		var tf:TextField = new TextField();
		tf.text = "B L O O P!";
		this.graphics.beginFill(0xffffff, 1);
		this.graphics.drawEllipse(0, 0, 80, 30);
		this.graphics.endFill();
		tf.x = 10;
		tf.y = 10;
		
		addChild(tf);
	}
	
}