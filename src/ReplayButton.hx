package ;

import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

/**
 * ...
 * @author eg
 */
class ReplayButton extends Sprite
{

	
	public function new() 
	{
		super();
		this.graphics.beginFill(0xcccccc, 1);
		this.graphics.drawRoundRect(0, 0, 150, 30, 5, 5);
		this.graphics.endFill();
		var tf:TextField = new TextField();
		tf.width = 150;
		tf.mouseEnabled = false;
		tf.autoSize = TextFieldAutoSize.CENTER;
		tf.text = "Play again";
		addChild(tf);
		this.buttonMode = true;
	}
	
}